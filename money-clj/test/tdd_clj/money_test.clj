(ns tdd-clj.money-test
  (:require [clojure.test :refer :all]
            [tdd-clj.money :refer :all]))

;; TODO
;; - [x] $5 + 10 CHF = $10 (レートが2:1の場合)
;; - [x] $5 + $5 = $10 
;; - [x] $5 + $5 がMoneyを返す
;; - [x] Bank.reduce(Money)
;; - [x] Moneyを変換して換算を行う
;; - [x] Reduce(Bank, String)
;; - [x] Sum.plus
;; - [x] Expression.times

(deftest testMultiplication
  (testing "$5 * 2 = $10"
    (let [five (dollar 5)]
      (do
        (is (= (dollar 10) (times five 2)))
        (is (= (dollar 15) (times five 3)))))))

(deftest testEquality
  (testing "等価性"
    (do
      (is (= (dollar 5) (dollar 5)))
      (is (not= (dollar 5) (dollar 6)))
      (is (not= (franc 5) (dollar 5))))))

(deftest testCurrency
  (testing "currency"
    (do
      (is (= :USD (:currency (dollar 1))))
      (is (= :CHF (:currency (franc 1)))))))

(deftest testSimpleAddition
  (testing "$5 + $5 = $10"
    (let [five (dollar 5)
          sum (plus five five)
          reduced-money (reduce-from-bank (->Bank {}) sum :USD)]
      (is (= (dollar 10) reduced-money)))))

(deftest testPlusReturnsSum
  (let [five (dollar 5)
        sum (plus five five)]
    (is (= five (:augend sum)))
    (is (= five (:addend sum)))))

(deftest testReduceSum
  (let [sum (->Sum (dollar 3) (dollar 4))
        bank (->Bank {})]
    (is (= (dollar 7) (reduce-from-bank bank sum :USD)))))

(deftest testReduceMoney
  (let [bank (->Bank {})]
    (is (= (dollar 1) (reduce-from-bank bank (dollar 1) :USD)))))

(deftest testReduceMoneyDifferentCurrency
  (let [bank-init (->Bank {})
        bank (add-rate bank-init :CHF :USD 2)]
    (is (= (dollar 1) (reduce-from-bank bank (franc 2) :USD)))))

(deftest testIdentityRate
  (is (= 1 (get-rate (->Bank {}) :USD :USD))))

(deftest testMixedAddition
  (testing "$5 + 10 CHF = $10 (レートが2:1の場合)"
    (let [fiveBucks (dollar 5)
          tenFrancs (franc 10)
          bank (add-rate (->Bank {}) :CHF :USD 2)]
      (is (= (dollar 10) (reduce-from-bank bank (plus fiveBucks tenFrancs) :USD))))))

(deftest testSumPlusMoney
  (let [fiveBucks (dollar 5)
        tenFrancs (franc 10)
        bank (add-rate (->Bank {}) :CHF :USD 2)
        sum (plus (->Sum fiveBucks tenFrancs) fiveBucks)]
    (is (= (dollar 15) (reduce-from-bank bank sum :USD)))))

(deftest testSumTimes
  (let [fiveBucks (dollar 5)
        tenFrancs (franc 10)
        bank (add-rate (->Bank {}) :CHF :USD 2)
        sum (times (->Sum fiveBucks tenFrancs) 2)]
    (is (= (dollar 20) (reduce-from-bank bank sum :USD)))))
