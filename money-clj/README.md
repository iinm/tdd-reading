# tdd-clj

## Installation

require [leiningen](https://leiningen.org/)

## Usage

```sh
# test
lein test

# run test when source is updated
lein test-refresh

# coverage test
lein cloverage
```
