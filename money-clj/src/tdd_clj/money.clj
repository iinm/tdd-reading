(ns tdd-clj.money
  (:gen-class))

(defprotocol ExpressionProtocol
  (reduce-money [this bank currency])
  (plus [this that])
  (times [this multiplier]))

(defprotocol BankProtocol
  (reduce-from-bank [this expression currency])
  (add-rate [this from to rate])
  (get-rate [this from to]))

(defrecord Money [amount currency])

(defn dollar [amount] (->Money amount :USD))

(defn franc [amount] (->Money amount :CHF))

(defrecord Sum [augend addend])

(extend-type Money
  ExpressionProtocol
  (reduce-money [this bank to]
    (let [rate (get-rate bank (:currency this) to)]
      (->Money (/ (:amount this) rate) to)))
  (plus [this that]
    (->Sum this that))
  (times [this multiplier]
    (update this :amount (partial * multiplier))))

(extend-type Sum
  ExpressionProtocol
  (reduce-money [this bank to]
    (->Money
      (+ (:amount (reduce-money (:augend this) bank to))
         (:amount (reduce-money (:addend this) bank to)))
      to))
  (plus [this that]
    (->Sum this that))
  (times [this multiplier]
    (->Sum (times (:augend this) multiplier) (times (:addend this) multiplier))))

(defrecord Bank [rates]
  BankProtocol
  (reduce-from-bank [this expression currency]
    (reduce-money expression this currency))
  (add-rate [this from to rate]
    (->Bank (assoc (:rates this) [from to] rate)))
  (get-rate [this from to]
    (if (= from to) 1 (get-in this [:rates [from to]]))))
