#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import inspect
import traceback


class TestCase:
    def __init__(self, name):
        self.name = name

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def run(self, result):
        result.testStarted()
        try:
            self.setUp()
            method = getattr(self, self.name)
            method()
        except Exception as e:
            tb = traceback.format_exc()
            print('[FAILED] ({}) {}'.format(self.name, tb), file=sys.stderr)
            result.testFailed()
        finally:
            self.tearDown()


class TestResult:
    def __init__(self):
        self.runCount = 0
        self.errorCount = 0

    def testStarted(self):
        self.runCount += 1

    def testFailed(self):
        self.errorCount += 1

    def summary(self):
        return "{} run, {} failed".format(self.runCount, self.errorCount)


class TestSuite:
    def __init__(self, testCase=None):
        self.tests = []
        if testCase is None:
            return
        for name, f in inspect.getmembers(testCase, predicate=inspect.isfunction):
            if name.startswith('__') or name in ('setUp', 'run', 'tearDown'):
                continue
            self.tests.append(testCase(name))

    def add(self, test):
        self.tests.append(test)

    def run(self, result):
        for test in self.tests:
            test.run(result)


class WasRun(TestCase):
    def setUp(self):
        self.log = "setUp "

    def testMethod(self):
        self.log += "testMethod "

    def testBrokenMethod(self):
        raise Exception

    def tearDown(self):
        self.log += "tearDown "


class BrokenSetup(TestCase):
    def setUp(self):
        raise Exception

    def testMethod(self):
        pass


class TestCaseTest(TestCase):
    def setUp(self):
        self.result = TestResult()

    def testTemplateMethod(self):
        test = WasRun("testMethod")
        test.run(self.result)
        assert("setUp testMethod tearDown " == test.log)

    def testFailedTemplateMethod(self):
        test = WasRun("testBrokenMethod")
        test.run(self.result)
        assert("setUp tearDown " == test.log)

    def testResult(self):
        test = WasRun("testMethod")
        test.run(self.result)
        assert("1 run, 0 failed" == self.result.summary())

    def testFailedResultFormatting(self):
        self.result.testStarted()
        self.result.testFailed()
        assert("1 run, 1 failed" == self.result.summary())

    def testFailedResult(self):
        test = WasRun("testBrokenMethod")
        test.run(self.result)
        assert("1 run, 1 failed" == self.result.summary())

    def testBrokenSetup(self):
        test = BrokenSetup("testMethod")
        test.run(self.result)
        assert("1 run, 1 failed" == self.result.summary())

    def testSuite(self):
        suite = TestSuite()
        suite.add(WasRun("testMethod"))
        suite.add(WasRun("testBrokenMethod"))
        suite.run(self.result)
        assert("2 run, 1 failed" == self.result.summary())


if __name__ == "__main__":
    suite = TestSuite(TestCaseTest)
    result = TestResult()
    suite.run(result)
    print(result.summary())
