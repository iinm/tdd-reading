# tdd-reading

## Purpose

[テスト駆動開発](https://estore.ohmsha.co.jp/titles/978427421788P) を学ぶ。

## Index

| path          | description            |
| ---           | ---                    |
| `./money-clj` | 第一部の多国通貨の実装 |
| `./xunit`     | 第二部のxUnitの実装    |
